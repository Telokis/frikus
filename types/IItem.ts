export interface IStat {
    name: string;
    from: number;
    to?: number;
}

export interface IRecipe {
    name: string;
    id: number;
    url: string;
    imgUrl: string;
    type: string;
    lvl: number;
    quantity: number;
}

export default interface IItem {
    _id: number;
    name: string;
    lvl: number;
    type: string;
    imgUrl: string;
    url: string;
    description: string;
    stats: IStat[];
    condition: string[];
    recipe: IRecipe[];
    setId: number;
    title: string;
    noAccents: string;
}
