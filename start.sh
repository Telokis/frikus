#!/bin/sh

loop()
{
    CMD=$@
    echo "> $CMD"
    $CMD
    while [ $? -ne 0 ]; do
        echo "Failed... Retrying in 3s..."
        sleep 3
        echo
        echo "> $CMD"
        $CMD
    done
}

if [ ! -d "/app/static/dofapi" ]; then
    echo "Executes retrieves Dofus data"
    loop npx jmake retrieve-data $DATA_URL
fi

echo "Starting the application"
echo "npm run start"
npm run start