import { RenderFunction, storiesOf } from "@storybook/react";
import React from "react";

import ItemSearch from "./ItemSearch";

const CenterDecorator = (story: RenderFunction) => (
    <div
        style={{
            marginTop: "50px",
            textAlign: "center",
        }}
    >
        {story()}
    </div>
);

storiesOf("ItemSearch", module)
    .addDecorator(CenterDecorator)
    .add("basic", () => (
        <ItemSearch
            onSelect={() => {}}
            data={[
                {
                    _id: 14084,
                    imgUrl:
                        "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAIAAACRXR/mAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAVMSURBVFjDY/g/KAHDqLNGnTXqrBHrrF9//n77/uP3nz+DwlmnTp2cMbGnPDelNCO2ND16zrTJA+ysc2fPTuvvmNJR01qW1V1b1NdQOru39dCenQPprCtXLldkxTWXZpw9fmhKR113XdGymZOmdTZeu3xhwJz1+PGTiqz4nvri7rriuvzkt29eLZ85ce3SBWvnTn70+PHAOOvDp8+VuUk9dcU99SVdtUVAxrYVC4DiB3ZuvXvj2sDkxN9//1UXpHXXFkLdVF+yd92SJbOmvHj6BCj7+fPngXFWQ3l+V00BxE3A9HRy54aehsq92za+fvXy8d1bA1NudTZUdVTlQdzUVVN46eD2vqaaLauXHT+wd9HUnlOH9w+Asyb1drRVZEPc1FFdcOP43olt9asWzFo+d1pfY+XmZfN//PpNb2ctmjerpTQD4qb2qvx7Zw5N7Wye2dsGRMBCob+98e8/ulc+G9evaShMhbiprTLvyaXj07tbZ/S0zuprn9Hd3NfaOAB14qGDB2rzkuBuenHt9My+Nribeol2EzWddfXKlcrseIibWityX984O29S9/TuFpCbupr72ppo2II4evTomrUr9u3fg16UP3laDi7KIW56e/v8kukTpnU1A900vaupr72JVE+S4KyMrEQjM3m/EJPDR3etXbcKLv7l67eaglRgCQ50U0t5zoe7F1fNnTq1swnipgldrTRsb928eUtbXzIg2DYq3uno8T0hkY5wqeggn7ykyO76YmA4Ad20cfHsKR2NEDdN7G6jbTNw8eL5ljYaLu4mqVl+QeE2Xv6mZTUVjp62PmEOQWHOEf6eEf4eH+5e2rlq0aT2BgrdRIKzjh47amAs5+ph6uRqFBnvZu1uHWcvkm/Jmm7GnhmuHxDtvGzm5JWzJ02BxF0nRW4iLW0FBrubmCsamSob2hhVufAkajA0hPLOKhINlGKIDjLMz4vrrima3d8JLDOn9HXTtS2/e/euuqb6EHupLF2GXEeOORXS69pkc105/DSYo5J9+xvKgG6aNqF3ALoYSVnJBSYMqVasS+pkupJEsjx5V9RJhmoxhMZ7Tm+r6etoGZiej7GtSZcXw9Y+xYZoofJQgY5Yge40ETdlhpyiWFdrw9lzZw+MswwsdfP1GXoyxCdkidWH8E7ME/eXZYh0U7E0VmRgYHj2/PkAOCvURNjSRD7Pgc9bgaEzWWR6saS/HEOoJkNseoCVjbqjsx29u68//v4P1GbzlGXIsWQ1c7crMGUocWUJU2MI1mWNS/MLinAMCnfw9LH0C7aYMq33759/9HAWsM3mo8boq8hQaMG4IoChyJHHyNXWxlrN1kbVxEZXSlZAx0DR2c0kIsaxoiYFqL60PIvmzvry45+bEkOEOkOKIUO/G8NEd8YFvgxTLRl6yxO6+/vTM5Lnzp3t5Grm7GYcFuUcleA0aWpjXKI/DZ21bPliaycTGxmGcHUGfw2GRf6MUz0ZOpwZmo0ZplTEIat09bB0cjMC1gGhUQ5tnRV1DRW0ctby5Yt1DKS8gu3DTXhidEBB1e3C0AV0kxHDzOp4NMVt7Y3GZgq2DjrAelNbT+YP7dKWqbkaMF4CQh1i4pwilBlSDIChxTDRgmFJdzFW9S2tdTV1JdU1JZ8/f6FVTvz06Yu2vhQwUrz9rVJzAyOj7D2VGZpNGdbOaP1PF4AztIxMlewcdc2t1MJi7AMjrCbN6rDzsRj4Ybf2jqaS8ozde7bOmNX37/+fxUunzZk3fVCMBvb2dcTGB65dt7yoJKOnt31wDVI+e/b8z5+/I2LsdNRZo84addaQcBYA2hWdyBiMai8AAAAASUVORK5CYII=",
                    noAccents: "Epee du Granduk",
                    title: "Épée du Granduk",
                },
                {
                    _id: 1720,
                    imgUrl:
                        "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAIAAACRXR/mAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAoUSURBVFjD7dl5UNNXHgDw/tfddqsIQkAIAmIOkkBCCAktonJJuFfAhCMJRxIg4Qi5yEFISEg4giI3IRxyyyWgiFjlsCKi1bbaa1dt8UJs1U5tt1PbqtmX0tud2fH2D5nvMJlMZvKZ7/t+3+/7Xl4xvZB/r7xkvWS9cKy7983xorCuXvuyviJhpwpfrfaq1pCM1YzZo4efM2vh4mKlEjvYtHG4NXisgzxoDOyt8+uv967Vbjx0cOD5sH68Z6otpw41bRrdRa7T+ebQkZwEhEbsrRERKgoIdRqP8gKfmemDz4j13ff39+/tVoojsmkOjaWkkdYQQ7nfJte/ReMtovErwzwt0ra6AJwu37uIjyvMdlHL4r64cfspso7PHS0rouUmr1XyECUSPIeG6DcEgVRJWKgo/MpYHytzkCz/SbAI9ViREuVcJvcBODXfPSvJrrO97smzRoba+WwvaaaTWoQTp2PENNfEzdYx+Nd3G4IBS52LjcCt+IX1B1wYfhUvDVMmJ2nFXrJM50Jx3J0fnxBrqL85iw5T8uCKPJyG75cXuVpNsSmhQ0uTHSlvrqxVk4ZbQ3oagkIwb8SQLP8o20q0TPRbzQywSg2HLq+pmo/OYsDm5+cfl8VKcJNnQsVsFz4VWhBnp01cs7s0pq+CWhBjUUS1VtFcR7S4noaN0yOpfCZqE+KNaAKQmXHLxKxw28xQ28wQG4a/tSjdvVRGKhbhhUzHkiL2T/ceg6XXckpV9MmJ/YXsTbKoV7U0BxXVRs9GvH+wsqUibVDm/O/+pMnRwtmh9Klq9PFqGJNsG4xZGUNcBVhUXytu2M8sc0DSAqzYW51LZGBBCUV5blkM+MzM9BMo+bF9I6KI17Q0qI5mzwl+rbuOdvfLdy7MVt65Pn1zinlzLGSxG3GjD67jQKM32oZjV4Dy4ob/xvolbfRASAEPXyIlFos8xSxHjSLl+x8euxNbajQaioWU4pDLwqqY0IuH+GC7MJmWbp2pr1f6d+Tb356imb79qKkQk52KCsWvYgVZ/4kFggxJ3mzFjYeVmpuUoM5DZdHXjY+PPRbrs4Wr3C2vqsXE3HT84XKX6wdiTF/Pjnfy2xr4vHQfRgK+W7jC9O1Z8EmDyEEr90mPXQdKHqzgn3GQjC3W9CBbCQcLqk0rwuezHVWy5J/uPirr8NtjMi6skE+Q09Z+sRt5bYA4sEvBTcYlxzqnUdaz4mFx0bCWnL+b7lwwy4R2xTIfEdudsXk1SNKDaUvzt2JGrS2WEM0bb55bdipu4dLSQ7M+/fRTfpqTIsctL9xiqtT1cifsg6H06GA7JhWA4EwqPI1q/h8bCTNwXzUt7V+WqfJ9NGJicogdJ8Tmr7Jfm1TKxZqrTYjNZqw7dfrMw7GM9Soe/R+VJaLqctU549rLXUgl1y+Fst4MosB4qej8TA8QYCOgU9y0jNe/OcI03f6gT+FQIAQyUjx5bVrgak7oA2kLhaT6W+UykD9vH57ZdOdz5y89BOvePdP8/Enz1LCw8LkR8p7BMznUJpWKyElBgVUAATpfJ/YuBjunhCjnEYoynK91uCz14d7WI6U5nlwGhpWASgyw5YRYP5g2UILcBJhZJsRyGagHHwb//1E9d+TAxWab9+rgWZGruSmYMnO3E5ZDku2pzUWrcj0ATiXw1ovdFzqQ13vgjcpQaoQTJwmWk4ZNinRJCfgfaWP6W/HT0DqJt4qHqCwXPjQL7M8TpchrvYjBAqecGDtNPmnZBFphfzn+Rs+6822YrFR3rdhbyiPu1SIWu1x7a9hLN+/nZoTSoyA5Kej0JHRi0BrQj39ZTbB3qAQEjRCvzIV/9fWdh563PvnX+TM7Vy7tRtdy1nC3OeqkZpkijzBTBjXd/fay0aIgaY1CQFJme1zrcfusFWbUE+u3J4D5bHLy0LYwx4x4F5A2WrTrX6otA7RFrLNaSGit8Do0MfAoY+DUvpbzjZaLvejSZAg/Ga6TkNRC72I2xPTjlR9ufXyu0Y4daTeuXX+jHz1R6THYFgamxRoN8b33zY2mkjETI6xzUjBp8W70AOvfZWQII8AGVNie5o29HepHnE4PdCsXjDZXutFauo0AyKQkeR6hW77u1kT0hTbY1R60mmG7T+2SG2VnKPMbbQ8FI/WuHbjWRqm5QOdmKaG2WQw3dhKG7m/zqwxC9bXsMwQPGPx62uSPPstPNLMutdoudKLKUyGZcU6gznI5hDah46Uu1OVu9JR+PTfSekqHyo+2KeF7jHaEDRqDBw2+Veo3T56Y/er2/VQKJouOYCWiQROYF3GLTXqkw77OsP6Gt4b7ax/r5LOnMedz42pQZ7sEDnnxjkMa5JVu1DsV688aEFUcew3d/mwdtlPgqqLYiSmOnXWBYD4Daeup8TJWxo2MDjMTfTg0VGLkOlaAVaLvqma9356WLS3lHmc/PPe458Thrh0fVlsu9rgt9qCudsGXeuADani3cP2xcvSZWuyxCowmCVoYZ2sofnOwKUQnwu9pI+8BuCb/oaYNBn2gMg8vzcJLWG4DzSHg/aEm32o9+8kcX6dnZiZ1thcaV01sf0vI8lIKcAU8Lx1jbV2GkzLeoYhqt13sub8rolJOVMZC5NvsK6T4AWPwcBt5uHXLSFsICDB8g9d9dUR9of/Nr757YqfqpRv/OXnqg+WNTc4nawRIhcCbGQEtSVyjZsHGuiIMpRulWyFauqOaalelILZXBRZx3Ix6v6FWMjg+AWWZzL2zo/neU72DqCzPp5CtuMkYRpTLaFtoZ22QPgVanQ6SZy+hQvd3Re6QegGfhmrHiXKQZOEkHAw/3edZXI1MT09uC3dt37m5z0gekCNPVWJOV3n0i13LxPi9HeGCGHuwvqp4e3EKAjxSNQIPlYTyjG5supp4wy1BzTKPk9vRs3oMKP8DKkSvgbxTQezkuRwpQ09rkQK2u9Y8cqGr9IJnwZqZHu+tIYAGPF6GBCYQx/XoTiV2qDW8V4F+dwdmrgJztNRNmutVkI0DrJod+U+dBYaTWjWhvSYQpAd8/TLrhB5l1Pm21QRNahDH9GbWXgVCJSZmJ8GLhR764vSnzupqKRw0bGgXOx4tsp1U2h7TOc5XuI0r4T0GcqvW50QFypy8CgyouaJ8Ug7FuUjgKeJueLqshYuLTcWw2uINb4/tnT/90fzpj0eHertEqCEVcqAlrFeFBau5zNotQ6rEJCnNRcxC5ac7f/zJ+afIuvPDvWpdzPi+Pw0nN78xVeV7DbaQhxWIeT38WKnrCT2sXw4WkSRJcsqLtiwWuSuE0c/h7rSufGtnzaZBnmWnJmpPQ97ADnpz9poikZc4jdDdUCLMJLBi37j+xdfPlHX3vqmmyKtBg/v8yu/XXQcPHpRlQOurJOD1lzduHZ+bfQ7Z2qmlbNem/vGdd0+dEqVaqmWUF+4CXKdMnZt75+XPBS9ZL1kvPuu/KgJOMJcuG9kAAAAASUVORK5CYII=",
                    noAccents: "Puissance de Crocoburio",
                    title: "Puissance de Crocoburio",
                },
            ]}
        />
    ));
