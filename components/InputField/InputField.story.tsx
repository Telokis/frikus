import { RenderFunction, storiesOf } from "@storybook/react";
import React from "react";

import InputField from "./InputField";

const CenterDecorator = (story: RenderFunction) => (
    <div
        style={{
            marginTop: "50px",
            textAlign: "center",
        }}
    >
        {story()}
    </div>
);

storiesOf("InputField", module)
    .addDecorator(CenterDecorator)
    .add("basic", () => (
        <InputField
            label="Quantité"
            id="basic-test"
            inputProps={{
                type: "number",
            }}
        />
    ))
    .add("label up", () => (
        <InputField
            label="Hello"
            id="basic-test"
            inputProps={{
                type: "text",
                value: "toto",
            }}
        />
    ));
