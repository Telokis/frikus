import NextLink from "next/link";
import React from "react";
import { Icon } from "semantic-ui-react";

interface ILinkProps {
    to: string;
    external: boolean;
    children?: React.ReactNode;
}

export default class Link extends React.Component<ILinkProps> {
    public static defaultProps = {
        external: false,
    };

    public render() {
        const { external, children, to, ...otherProps } = this.props;

        if (external) {
            return (
                <a {...otherProps} href={to} rel="noopener noreferrer" target="_blank">
                    {children}
                    <Icon name="external" size="small" style={{ marginLeft: "0.3rem" }} />
                </a>
            );
        }

        return (
            <NextLink {...otherProps} href={to}>
                <a>{children}</a>
            </NextLink>
        );
    }
}
