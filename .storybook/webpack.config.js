const path = require("path");

module.exports = ({ config, mode }) => {
    config.module.rules.push({
        test: /\.(ts|tsx)$/,
        use: [
            {
                loader: require.resolve("babel-loader"),
                options: {
                    presets: [["react-app", { flow: false, typescript: true }]],
                },
            },
        ],
    });
    config.resolve.alias.$components = path.join(__dirname, "..", "components");
    config.resolve.alias.$helpers = path.join(__dirname, "..", "helpers");
    config.resolve.extensions.push(".ts", ".tsx");
    return config;
};
