import { configure } from "@storybook/react";
import "../static/semantic.readable.min.css";

function loadStories() {
    const req = require.context("../components", true, /\.story\.tsx$/);
    req.keys().forEach(filename => req(filename));
}

configure(loadStories, module);
