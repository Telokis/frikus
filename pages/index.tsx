import { Card, Segment } from "semantic-ui-react";

import Link from "$components/Link";

const Index = () => (
    <Segment raised>
        <Card.Group centered>
            <Link to="/my-items">
                <Card
                    link
                    header="Mes objets"
                    description="Consulter votre historique et voir les objets actuellement en vente."
                />
            </Link>
            <Link to="/add-items">
                <Card
                    link
                    header="Ajouter des objets"
                    description="Créez de nouveaux objets pour faire le point sur vos gains."
                />
            </Link>
        </Card.Group>
    </Segment>
);

export default Index;
