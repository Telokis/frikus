import App, { Container } from "next/app";
import Head from "next/head";
import React, { ReactNode } from "react";
import withStyle, { WithStyles } from "react-jss";
import { Container as SemanticContainer, Header, Menu, Segment } from "semantic-ui-react";

import Link from "$components/Link";

const styles = {
    "@media only screen and (min-width: 1800px)": {
        contentContainer: {
            width: "1854px!important",
        },
    },
    contentContainer: {
        marginTop: "4em",
    },
};

interface ILayoutProps extends WithStyles<typeof styles> {
    children?: ReactNode;
}
const Layout = withStyle(styles)(({ children, classes }: ILayoutProps) => (
    <div>
        <Menu fixed="top" inverted>
            <SemanticContainer>
                <Menu.Item header>
                    <Link to="/">Frikus</Link>
                </Menu.Item>
                <Menu.Item>
                    <Link to="/add-items">Ajouter des objets</Link>
                </Menu.Item>
            </SemanticContainer>
        </Menu>

        <SemanticContainer className={classes.contentContainer}>{children}</SemanticContainer>

        <Segment inverted vertical style={{ margin: "5em 0em 0em", padding: "5em 0em" }}>
            <SemanticContainer textAlign="center">
                <Header inverted as="h4" content="Footer Header" />
                <p>
                    Extra space for a call to action inside the footer that could help re-engage
                    users.
                </p>
            </SemanticContainer>
        </Segment>
    </div>
));

export default class MyApp extends App {
    public componentDidMount() {
        const style = document.getElementById("server-side-styles");

        if (style) {
            style.parentNode!.removeChild(style);
        }
    }

    public render() {
        const { Component, pageProps } = this.props;

        return (
            <Container>
                <Head>
                    <link href="/static/semantic.readable.min.css" rel="stylesheet" />
                </Head>
                <Layout>
                    <Component {...pageProps} />
                </Layout>
            </Container>
        );
    }
}
